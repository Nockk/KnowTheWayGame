﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class OptionsMenu : MonoBehaviour
    {

        [SerializeField] private GameObject resumePanel = null;
        [SerializeField] private GameObject restartPanel = null;
        [SerializeField] private GameObject quitMenuPanel = null;
        [SerializeField] private GameObject quitPanel = null;
        [SerializeField] private GameObject MainPanel = null;
        [SerializeField] private GameObject[] spawn;
        [SerializeField] private bool IsActive = false;
        [SerializeField] private GameObject menu;
        public TcpClientController TcpClientController;

        public void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                IsActive = !IsActive;
                menu.SetActive(IsActive);
                Cursor.visible = IsActive;
                if (IsActive)
                    Cursor.lockState = CursorLockMode.None;
                else
                    Cursor.lockState = CursorLockMode.Locked;
                
                
            }
        }

        public void ResumeGame()
        {
            IsActive = false;
            menu.SetActive(false);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        public void restartGame()
        {
            menu.SetActive(false);
            for (int i = 0; i < spawn.Length; i++)
            {

            }
        }
        public void quitMenuGame()
        {
            MainPanel.SetActive(true);
            menu.SetActive(false);
        }
        public void QuitGame()
        {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
    }
