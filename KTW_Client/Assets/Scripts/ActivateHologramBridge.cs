﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHologramBridge : MonoBehaviour
{
    public GameObject BridgeToToggle;
    public Light[] Lights;
    private bool m_Status;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            BridgeToToggle.SetActive(true);
            for (int i = 0; i < Lights.Length; i++)
            {
                Lights[i].color = Color.green;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Player")
        {
            BridgeToToggle.SetActive(false);
            for (int i = 0; i < Lights.Length; i++)
            {
                Lights[i].color = Color.red;
            }
        }
    }
}
