﻿using Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Systems;
using Controllers;
using TMPro;
using UI.Menus;
using UnityEngine;
using UnityEngine.UI;

public class TcpClientController : MonoBehaviour
{

    [HideInInspector]
    public Common.Player Player;
    private Dictionary<Guid, GameObject> _playerGameObjectDict;

    public GameObject SpawnPoint;
    public GameObject PlayerPrefab;
    public GameObject ConnectionUI;
    public TextMeshProUGUI PlayerNameInputText;
    public string IpAddress;
    public int Port;
    public GameObject[] SpawnPoints;
    public GameObject[] players;
    private void Awake()
    {
        Player = new Common.Player();
        _playerGameObjectDict = new Dictionary<Guid, GameObject>();
        Player.GameState = GameState.Disconnected;
        Player.TcpClient = new TcpClient();
    }
    void Start()
    {

    }

    void Update()
    {
        if (Player.TcpClient.Connected)
        {
            switch (Player.GameState)
            {
                case GameState.Connecting:
                    Debug.Log("Connecting");
                    Connecting();
                    break;
                case GameState.Connected:
                    Debug.Log("Connected");
                    Connected();
                    break;
                case GameState.Sync:
                    Debug.Log("Syncing");
                    Sync();
                    break;
                case GameState.GameStarted:
                    Debug.Log("GameStarted");
                    GameStarted();
                    break;
            }
        }
        else
        {
            Debug.Log("Disconnected");
        }
        Debug.Log(_playerGameObjectDict.Count);

    }

    private void GameStarted()
    {
        if (Player.DataAvailable())
        {
            Message message = ReceiveMessage();
            if (message.MessageType == MessageType.NewPlayer)
            {
                GameObject playerGameObject = Instantiate(PlayerPrefab,
                    new Vector3(message.PlayerInfo.X, message.PlayerInfo.Y, message.PlayerInfo.Z),
                    Quaternion.identity);

                _playerGameObjectDict.Add(message.PlayerInfo.Id, playerGameObject);
            }
            else if (message.MessageType == MessageType.PlayerDisconnect)
            {
                Destroy(_playerGameObjectDict[message.PlayerInfo.Id]);
                _playerGameObjectDict.Remove(message.PlayerInfo.Id);
                Debug.Log(_playerGameObjectDict.Count);
            }
            else if (message.MessageType == MessageType.PlayerMovement)
            {
                if (_playerGameObjectDict.ContainsKey(message.PlayerInfo.Id) &&
                    message.PlayerInfo.Id != Player.Id)
                {
                    PlayerInfo playerInfo = message.PlayerInfo;
                    _playerGameObjectDict[playerInfo.Id].transform.position =
                        new Vector3(playerInfo.X, playerInfo.Y, playerInfo.Z);
                    Vector4 rotation = new Vector4(playerInfo.RotX, playerInfo.RotY, playerInfo.RotZ, playerInfo.RotW);
                    _playerGameObjectDict[playerInfo.Id].transform.rotation = new Quaternion(rotation.x, rotation.y, rotation.z, rotation.w);
                }
            }
        }
    }

    private void Sync()
    {
        if (Player.DataAvailable())
        {
            Message message = ReceiveMessage();
            Debug.Log(message.MessageType);
            // processar messages NewPlayer
            if (message.MessageType == MessageType.NewPlayer)
            {
                GameObject playerGameObject = Instantiate(PlayerPrefab,
                    new Vector3(message.PlayerInfo.X, message.PlayerInfo.Y, message.PlayerInfo.Z),
                    new Quaternion(message.PlayerInfo.RotX,message.PlayerInfo.RotY, message.PlayerInfo.RotZ, 1));
                _playerGameObjectDict.Add(message.PlayerInfo.Id, playerGameObject);
            }
            // processar messages PlayerMovement
            else if (message.MessageType == MessageType.PlayerMovement)
            {
                if (_playerGameObjectDict.ContainsKey(message.PlayerInfo.Id))
                {
                    _playerGameObjectDict[message.PlayerInfo.Id].transform.position =
                        new Vector3(message.PlayerInfo.X, message.PlayerInfo.Y, message.PlayerInfo.Z);
                    _playerGameObjectDict[message.PlayerInfo.Id].transform.rotation =
                        new Quaternion(message.PlayerInfo.RotX,message.PlayerInfo.RotY, message.PlayerInfo.RotZ, 1);
                }
            }
            else if (message.MessageType == MessageType.FinishedSync)
            {
                ConnectionUI.SetActive(false);
                players = GameObject.FindGameObjectsWithTag("Player");
                GameObject playerGameObject =
                    Instantiate(PlayerPrefab, SpawnPoints[0].transform.position, Quaternion.identity);

                if(players.Length == 0){
                    playerGameObject.transform.position = SpawnPoints[0].transform.position;
                    //Instantiate(PlayerPrefab, SpawnPoints[Player.Index - 1].transform.position, Quaternion.identity);
                }
                else
                {
                    playerGameObject.transform.position = SpawnPoints[1].transform.position;
                }
                playerGameObject.GetComponent<PlayerController>().TcpClientController = this;
                playerGameObject.GetComponent<PlayerController>().Playable = true;
                playerGameObject.GetComponent<PlayerController>().enabled = true;

                playerGameObject.GetComponent<PlayerCameraController>().TcpClientController = this;
                playerGameObject.GetComponent<PlayerCameraController>().Playable = true;
                playerGameObject.GetComponent<PlayerCameraController>().enabled = true;

                _playerGameObjectDict.Add(Player.Id, playerGameObject);

                Player.GameState = GameState.GameStarted;
            }
        }
    }


    private void Connected()
    {
        if (Player.DataAvailable())
        {
            Message message = ReceiveMessage();
            Debug.Log(message.Description);
            Player.GameState = GameState.Sync;
        }
    }

    private void Connecting()
    {
        if (Player.TcpClient.GetStream().DataAvailable)
        {
            string playerJsonString = Player.BinaryReader.ReadString();
            Common.Player player = JsonConvert.DeserializeObject<Common.Player>(playerJsonString);
            Player.Id = player.Id;
            Player.MessageList.Add(player.MessageList.FirstOrDefault());
            Player.Name = PlayerNameInputText.text;
            Player.Index = player.Index;

            Message message = new Message();
            message.MessageType = MessageType.PlayerName;
            Player.MessageList.Add(message);

            string newPlayerJsonString = JsonConvert.SerializeObject(Player);
            Player.BinaryWriter.Write(newPlayerJsonString);
            Player.GameState = GameState.Connected;
        }
    }

    private Message ReceiveMessage()
    {
        string msgJson = Player.BinaryReader.ReadString();
        Message msg = JsonConvert.DeserializeObject<Message>(msgJson);
        Player.MessageList.Add(msg);
        return msg;
    }

    public void SendMessage(Message message)
    {
        string messageJson = JsonConvert.SerializeObject(message);
        Player.BinaryWriter.Write(messageJson);
        Player.MessageList.Add(message);
    }

    public void StartTcpClient()
    {
        Player.TcpClient.BeginConnect(IPAddress.Parse(IpAddress), Port,
            AcceptConnection, Player.TcpClient);
        Player.GameState = GameState.Connecting;
    }

    private void AcceptConnection(IAsyncResult ar)
    {
        TcpClient tcpClient = (TcpClient)ar.AsyncState;
        tcpClient.EndConnect(ar);

        if (tcpClient.Connected)
        {
            Debug.Log("Client connected");
            Player.BinaryReader = new System.IO.BinaryReader(tcpClient.GetStream());
            Player.BinaryWriter = new System.IO.BinaryWriter(tcpClient.GetStream());
            Player.MessageList = new List<Message>();
        }
        else
        {
            Debug.Log("Client connection refused");
        }
    }

    void OnApplicationQuit()
    {

        Debug.Log("Disconnected");
        Disconnect();
    }

    private void Disconnect()
    {
        Player.GameState = GameState.Disconnected;
        Message msg = new Message();
        msg.MessageType = MessageType.Disconnect;
        PlayerInfo info = new PlayerInfo();
        info.Id = this.Player.Id;
        info.Name = this.Player.Name;
        info.Index = this.Player.Index;
        msg.PlayerInfo = info;
        this.SendMessage(msg);
    }
}
