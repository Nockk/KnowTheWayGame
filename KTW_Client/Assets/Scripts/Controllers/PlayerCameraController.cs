﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Player;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using Common;

namespace Controllers
{
    public class PlayerCameraController : MonoBehaviour
    {
        private Quaternion _oldRotation = Quaternion.identity;
        public bool Playable;
        public TcpClientController TcpClientController;
        [SerializeField] private float mouseSensitivity = 8.0f;

        [SerializeField, InspectorName("PlayerBody")] private Transform m_PlayerBody = null;
        [SerializeField, InspectorName("Camera")] private Camera m_Camera = null;

        private PlayerUIController m_PlayerUI;

        public Transform CameraTransform => m_Camera.transform;

        private float m_RotationX = 0.0f;

        private DJA m_Controls;

        private DJA Controls
        {
            get
            {
                if (m_Controls != null) return m_Controls;
                return m_Controls = new DJA();
            }
        }

        public void OnEnable() => Controls.Player.Look.Enable();

        public void OnDisable() => Controls.Player.Look.Disable();

        private void Start()
        {
            m_PlayerUI = GetComponent<PlayerUIController>();
            if (!Playable) return;

            m_Camera.gameObject.SetActive(true);
            enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        void FixedUpdate()
        {
            if (!Playable) return;

            Vector2 movement = Controls.Player.Look.ReadValue<Vector2>();

                movement.x *= mouseSensitivity * Time.deltaTime;
                movement.y *= mouseSensitivity * Time.deltaTime;

                m_RotationX -= movement.y;
                m_RotationX = Mathf.Clamp(m_RotationX, -90.0f, 90.0f);
                m_Camera.transform.localRotation = Quaternion.Euler(m_RotationX, 0f, 0f);
                m_PlayerBody.Rotate(Vector3.up * movement.x);

            if (transform.rotation != _oldRotation)
            {
                Message msg = new Message();
                msg.MessageType = MessageType.PlayerMovement;
                PlayerInfo info = new PlayerInfo();
                info.Id = TcpClientController.Player.Id;
                info.Name = TcpClientController.Player.Name;
                info.Index = TcpClientController.Player.Index;
                info.X = transform.position.x;
                info.Y = transform.position.y;
                info.Z = transform.position.z;
                info.RotX = transform.rotation.x;
                info.RotY = transform.rotation.y;
                info.RotZ = transform.rotation.z;
                info.RotW = transform.rotation.w;
                msg.PlayerInfo = info;
                TcpClientController.SendMessage(msg);
            }

            _oldRotation = transform.rotation;

        }

    }
}

