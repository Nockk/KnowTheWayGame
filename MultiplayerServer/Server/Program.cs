﻿using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            ServerController server = new ServerController();
            server.StartServer();
        }
    }
}
